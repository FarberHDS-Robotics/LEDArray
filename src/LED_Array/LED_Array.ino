/**
   This Program is for the Arduino platform with 8 LEDs and 8 resistors
    and one pin for each LED and one for ground
   The schematic can be found in the file: LED_Array_Schematic.png

   To Use:
      In the loop() function switch "outsideIn();" with one of the other
        functions in this file

   Program functions:
      setup()
      loop()
      outsideIn()
      lightUpAllThenTurnOffAll()
      chaseLeftToRight()
      chaseRightToLeft()
      allOn()
      allOff()
      alternate()

    Helper Functions:
      outsideInHelper(int pinNumber)

   Note: In the diagrams "|" means on and "-" means off

   Author:   Robert (Rafi) Lerman
   Version:  2016.09.07
   Class:    Robotics 8th hour
*/

// Global Variables
const int startPin = 6;
const int endPin = 13;

// Setup the required pins
void setup() {
  // Loop through the pins and initialize them
  for (int i = startPin; i <= endPin; i++)
  {
    // Initialize Pins
    pinMode(i, OUTPUT);
  }
}

// Function that gets looped and calls one of the other functions
void loop() {
  // Call the external function
  outsideIn();
  lightUpAllThenTurnOffAll();
  chaseLeftToRight();
  chaseRightToLeft();
  allOn();
  delay(2000);
  allOff();
  alternate();
}

// Does an outside in pattern
/*
      | - - - - - - |
      - | - - - - | -
      - - | - - | - -
      - - - | | - - -
      - - | - - | - -
      - | - - - - | -
      | - - - - - - |
*/
void outsideIn ()
{
  for (int i = 0; i < (endPin - startPin); i++)
  {
    if (i < ((endPin - startPin) / 2))
    {
      outsideInHelper(i);
    }
    else
    {
      outsideInHelper(6 - i);
    }
  }
}

// Function to simplify outsideIn()
//  Params:
//    int pinNumber  -  The number pin to light up
void outsideInHelper (int pinNumber)
{
  digitalWrite(startPin + pinNumber, HIGH);
  digitalWrite(endPin - pinNumber, HIGH);
  delay(1000);
  digitalWrite(startPin + pinNumber, LOW);
  digitalWrite(endPin - pinNumber, LOW);
}

// Lights all the pins and then turns them all off
/*
      | | | | | | | |
      Than:
      - - - - - - - -
*/
void lightUpAllThenTurnOffAll()
{
  allOn();
  delay(1000);
  allOff();
}

// Makes the LEDs light up from left to right
/*
      | - - - - - - -
      - | - - - - - -
      - - | - - - - -
      - - - | - - - -
      - - - - | - - -
      - - - - - | - -
      - - - - - - | -
      - - - - - - - |
*/
void chaseLeftToRight ()
{
  for (int i = startPin; i <= endPin; i++)
  {
    digitalWrite(i, HIGH);
    delay(1000);
    digitalWrite(i, LOW);
  }
}

// Makes the LEDs light up from right to left
/*
      - - - - - - - |
      - - - - - - | -
      - - - - - | - -
      - - - - | - - -
      - - - | - - - -
      - - | - - - - -
      - | - - - - - -
      | - - - - - - -
*/
void chaseRightToLeft ()
{
  for (int i = endPin; i >= startPin; i--)
  {
    digitalWrite(i, HIGH);
    delay(1000);
    digitalWrite(i, LOW);
  }
}

// Turn all the LEDs on
/*
      | | | | | | | |
*/
void allOn ()
{
  for (int i = startPin; i <= endPin; i++)
  {
    digitalWrite(i, HIGH);
  }
}

// Turns all the LEDs off
/*
      - - - - - - - -
*/
void allOff ()
{
  for (int i = startPin; i <= endPin; i++)
  {
    digitalWrite(i, LOW);
  }
}

// Lights up every other LED
/*
      - | - | - | - |
      | - | - | - | -
*/
void alternate ()
{
  int divisor = 0;
  for (int i = 0; i < 8; i++)
  {
    if (i % 2 == divisor)
    {
      digitalWrite(startPin + i, HIGH);
    }
    if (i == 6 && divisor == 0)
    {
      delay(2000);
      allOff();
      i = 0;
      divisor++;
    }
    if (i == 7)
    {
      break;
    }
  }
  delay(2000);
  allOff();
}
